﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping
{
    //[CreateAssetMenu(fileName = "BuildingPrefab", menuName = "MapUtility/BuildingDataSObject", order = 2)]
    public class BuildingDataSObject : ScriptableObject
    {
        public virtual void UpdateBuilding() { }
        public virtual void OnBuilt() { }
        public virtual void OnDestroyed() { }


        public float m_SecondsToGrow;
        public BuildingEnum m_AssignedValue;
        public int m_MovementDifficulty;
        public GameObject m_BuildingObject;
        
        [Tooltip("If this is true, the program will skip terrain check.")]
        public bool m_IsUniversal;
        
        [HideInInspector]
        public MapManager man;

        public BuildingCost[] buildCosts;
        public List<ResourceCost> m_ResourcesToUpdate;
        public List<TerrainEnum> m_CompatableTerrainTypes;

        public virtual List<ResourceCost> ProcessCosts() { return null; }
    }
}