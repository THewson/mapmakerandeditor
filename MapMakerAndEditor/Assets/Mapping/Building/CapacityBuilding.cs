﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping {
    [CreateAssetMenu(fileName = "CapacityBuilding", menuName = "MapUtility/CapacityBuilding")]
    public class CapacityBuilding : BuildingDataSObject {

        public List<ResourceMaxBooster> m_ResourceList;
        
        override public void OnBuilt()
        {
            man.m_ResourceManager.AddTotalResource(m_ResourceList);
        }

        public override void OnDestroyed()
        {
            man.m_ResourceManager.RemoveTotalResource(m_ResourceList);
        }
    }
}