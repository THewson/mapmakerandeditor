﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping
{
    public class BuildingPieceData : MonoBehaviour
    {
        public BuildingDataSObject m_BuildingData;
        public GameObject m_Building;
        public Vector3 m_TotalGrownSize;
        public Vector3 m_FinalLocalPosition;
        
        public float m_BuildingGrowth;

        private void LateUpdate()
        {
            if (m_BuildingData.m_AssignedValue != BuildingEnum.EMPTY)
            {
                if (m_BuildingGrowth < 1)
                {
                    m_BuildingGrowth += Time.deltaTime / m_BuildingData.m_SecondsToGrow;
                    
                    if (m_BuildingGrowth > 1)
                    {
                        m_BuildingGrowth = 1;
                        m_BuildingData.m_BuildingObject.transform.localScale = new Vector3(m_TotalGrownSize.x, m_TotalGrownSize.y, m_TotalGrownSize.z);
                        m_BuildingData.m_BuildingObject.transform.localPosition = new Vector3(m_FinalLocalPosition.x, m_FinalLocalPosition.y, m_FinalLocalPosition.z);

                        m_BuildingData.OnBuilt();
                    }
                    else
                    {
                    m_Building.transform.localScale = new Vector3(m_TotalGrownSize.x, m_TotalGrownSize.y * m_BuildingGrowth, m_TotalGrownSize.z);
                    m_Building.transform.localPosition = new Vector3(m_FinalLocalPosition.x, m_FinalLocalPosition.y * m_BuildingGrowth, m_FinalLocalPosition.z);
                    }

                }
                else
                {
                    m_BuildingData.UpdateBuilding();
                }
            }
        }

        public void CreateBuilding()
        {
            if(m_Building != null)
            {
                Destroy(m_Building);
            }

            if (m_BuildingData.m_AssignedValue != BuildingEnum.EMPTY)
            {
                GameObject go = Instantiate<GameObject>(m_BuildingData.m_BuildingObject, this.gameObject.transform);
                m_FinalLocalPosition = m_BuildingData.m_BuildingObject.transform.position;
                m_TotalGrownSize = m_BuildingData.m_BuildingObject.transform.localScale;

                m_Building = go;
            }
            else
            {
                m_BuildingGrowth = 1;
            }
        }
    }
}