﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping
{

    [System.Serializable]
    public struct ResourceCost
    {
        public ResourceType resourceType;
        public bool consumesResources;
        public float secondsPerTick;
        public int amountPerTick;
    };    

    [System.Serializable]
    public struct ResourceMaxBooster
    {
        public ResourceType resource;
        public int NumberToAdd;
    }

    [System.Serializable]
    public struct BuildingCost
    {
        public ResourceType resource;
        public int costAmount;
    }
}