﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mapping {
    [RequireComponent(typeof(Button))]
    public class OnClickMakeMap : MonoBehaviour {
        public Transform gameManager;

        public void clicked()
        {
            MapManager man = gameManager.GetComponent<MapManager>();

            man.InitialiseNewMap();
        }

    }
}