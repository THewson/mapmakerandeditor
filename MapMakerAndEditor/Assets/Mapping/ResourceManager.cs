﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mapping
{
    // Container for a given resource type
    public class ResourceContainer
    {
        // Resource type.
        public ResourceType resource;
        // Resources current amount
        public int resourceCurrent;
        // Resource maximum capacity
        public int resourceMax;
        // The text box it'll be displayed on
        public Text text;
        public void AddResource(int res)
        {
            resourceCurrent += res;
        }
        public void AlterMaxResource(int res)
        {
            resourceMax += res;
        }
    }

    public class ResourceManager : MonoBehaviour
    {
        [Header("Resource Lists")]
        public List<ResourceType> ResourceList;
        public List<int> ResourceStartingValues;
        public List<int> ResourceStartingMax;

        [Header("Resource Text Fields")]
        public List<Text> ResourceTextList;
        
        [HideInInspector]
        public List<ResourceContainer> m_ResourceList;
        
        // Use this for initialization.
        void Start()
        {
            m_ResourceList = new List<ResourceContainer>();

            // Checks to see that the starting max values are equal to the resource lists count.
            if (ResourceStartingMax.Count < ResourceList.Count)
            {
                int difference = ResourceList.Count - ResourceStartingMax.Count;
                for (int i = 0; i != difference; i++)
                {
                    ResourceStartingMax.Add(0);
                }
            }

            // Checks to see that the starting values are equal to the resource lists count and adds a default value of 0 to it.
            if (ResourceStartingValues.Count < ResourceList.Count)
            {
                int difference = ResourceList.Count - ResourceStartingValues.Count;
                for (int i = 0; i != difference; i++)
                {
                    ResourceStartingValues.Add(0);
                }
            }

            // Checks to see that the number of text slots are equal to the number of resources.
            if(ResourceTextList.Count < ResourceList.Count)
            {
                Debug.Log("There are more resources than text boxes. Some resources won't display correctly.");
            }
            
            // This loop creates a resource container, fills it with the correct information for each slot, and adds it to the resource list.
            if (ResourceList.Count == ResourceStartingValues.Count)
            {
                for (int i = 0; i != ResourceList.Count; i++)
                {
                    ResourceContainer res = new ResourceContainer();
                    res.resource = ResourceList[i];
                    res.resourceCurrent = ResourceStartingValues[i];
                    res.resourceMax = ResourceStartingMax[i];
                    res.text = ResourceTextList[i];
                    m_ResourceList.Add(res);
                }
            }
        }

        // Update the relevant resources.
        public void UpdateResourceTicks(List<ResourceCost> resourceCostList)
        {
            for(int i = 0; i!= resourceCostList.Count; i++)
            {
                if (resourceCostList[i].consumesResources)
                {
                    for(int x = 0; x!= m_ResourceList.Count; x++)
                    {
                        if(m_ResourceList[x].resource == resourceCostList[i].resourceType)
                        {
                            m_ResourceList[x].AddResource(-resourceCostList[i].amountPerTick);
                        }
                    }
                }
                else
                {
                    for (int x = 0; x != m_ResourceList.Count; x++)
                    {
                        if (m_ResourceList[x].resource == resourceCostList[i].resourceType)
                        {
                            m_ResourceList[x].AddResource(resourceCostList[i].amountPerTick);
                        }
                    }
                }
            }
        }

        public void AddTotalResource(List<ResourceMaxBooster> list)
        {
            for(int i = 0; i!= list.Count; i++)
            {
               for(int res = 0; res != m_ResourceList.Count; res++)
                {
                    if(list[i].resource == m_ResourceList[res].resource)
                    {
                        m_ResourceList[res].resourceMax += list[i].NumberToAdd;
                        break;
                    }
                } 
            }
        }

        public void RemoveTotalResource(List<ResourceMaxBooster> list)
        {
            for (int i = 0; i != list.Count; i++)
            {
                for (int res = 0; res != m_ResourceList.Count; res++)
                {
                    if (list[i].resource == m_ResourceList[res].resource)
                    {
                        m_ResourceList[res].resourceMax -= list[i].NumberToAdd;
                        break;
                    }
                }
            }
        }


        private void LateUpdate()
        {
            // Updates the resource displayer
            for (int i = 0; i != m_ResourceList.Count; i++)
            {
                if(m_ResourceList[i].resourceCurrent > m_ResourceList[i].resourceMax)
                {
                    m_ResourceList[i].resourceCurrent = m_ResourceList[i].resourceMax;
                }

                m_ResourceList[i].text.text = System.Enum.GetName(typeof(ResourceType), m_ResourceList[i].resource) + 
                    ": " + m_ResourceList[i].resourceCurrent + " / " + m_ResourceList[i].resourceMax;
            }
        }

        // Adds a certain amount to a given resource.
        public void AddResource(ResourceType resource, int resourceToAdd)
        {
            int index = -1;
            for(int i = 0; i != m_ResourceList.Count; i++)
            {
                if(m_ResourceList[i].resource == resource)
                {
                    index = i;                    
                    if(m_ResourceList[i].resourceCurrent + resourceToAdd <= m_ResourceList[i].resourceMax)
                    {
                        m_ResourceList[i].AddResource(resourceToAdd);
                    }
                }
            }
            if(index == -1)
            {
                Debug.Log("The resource you are trying to access doesn't exist.");
            }
        }
        
        // Removes a certain amount from a given resource.
        public void RemoveResource(ResourceType resource, int resourceToRemove)
        {
            int index = -1;
            for (int i = 0; i != m_ResourceList.Count; i++)
            {
                if (m_ResourceList[i].resource == resource)
                {
                    index = i;
                    if (m_ResourceList[i].resourceCurrent != 0)
                    {
                        m_ResourceList[i].AddResource(-resourceToRemove);
                    }
                }
            }
            if (index == -1)
            {
                Debug.Log("The resource you are trying to access doesn't exist.");
            }
        }

        // Print the resource list.
        public void PrintResourceList() {
            for (int i = 0; i!= m_ResourceList.Count; i++)
            {
                Debug.Log(i + ": " + m_ResourceList[i].resource + " : " + 
                    m_ResourceList[i].resourceMax + " : " + 
                    m_ResourceList[i].resourceCurrent);
            }
        }
    }
}

