﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mapping
{
    public class MapManager : MonoBehaviour
    {
        // MAP MANAGING //
        [Header("Prefabs")]
        public GameObject m_BaseTerrainPiece;
        public MapDataSObject[] m_TerrainPrefabs;
        public BuildingDataSObject[] m_BuildingPrefabs;
        
        [Header("Input Fields")]
        public InputField m_InputFieldWidth;
        public InputField m_InputFieldHeight;

        public InputField m_SaveLoadInputBox;

        [Header("Misc.")]
        public ResourceManager m_ResourceManager;

        public Transform m_ParentFolder;
        public Transform m_Camera;
        public Vector3 offSet = new Vector3(3, 0, 3);
        
        public Map c_Map;
        private CameraController m_CameraController;
        
        void Start()
        {
            c_Map = new Map();
            if (m_Camera.GetComponent<CameraController>())
            {
                m_CameraController = m_Camera.GetComponent<CameraController>();
            }
        }

        public string SaveLoadText()
        {
            if (m_SaveLoadInputBox.text == "")
            {
                return "ERRORINVALIDSAVENAME321";
            }
            else
            {
                return m_SaveLoadInputBox.text;
            }
        }

        // Extend this area for custom building types
        void CalculateResourceUpdates()
        {
            List<ResourceCost> resourceCost = new List<ResourceCost>();

            for(int x = 0; x!= c_Map.c_Mapsize.x; x++)
            {
                for(int y = 0; y != c_Map.c_Mapsize.y; y++)
                {
                    if (c_Map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingData.m_ResourcesToUpdate.Count > 0)
                    {
                        if (c_Map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingGrowth == 1)
                        {
                            foreach (ResourceCost rs in c_Map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingData.m_ResourcesToUpdate)
                            {
                                resourceCost.Add(rs);
                            }
                        }
                    }
                }
            }            
            m_ResourceManager.UpdateResourceTicks(resourceCost);
        }

        public void LateUpdate()
        {
            CalculateResourceUpdates();
        }

        // Destroys the current map
        public void DestroyMap()
        {
            while (m_ParentFolder.childCount != 0)
            {
                GameObject.DestroyImmediate(m_ParentFolder.GetChild(0).gameObject);
            }
        }
        
        // Initialise New Map
        public void InitialiseNewMap()
        {
            DestroyMap();
            int width = int.Parse(m_InputFieldWidth.text);
            int height = int.Parse(m_InputFieldHeight.text);
            
            m_CameraController.SnapCameraToMapMiddle(width, height, offSet);

            if (width <= 100 && height <= 100)
            {
                c_Map.c_Mapsize.x = width;
                c_Map.c_Mapsize.y = height;

                c_Map.c_Map = new GameObject[width, height];
                c_Map.c_TerrainDataMap = new MapPieceData[width, height];
                c_Map.c_BuildingMap = new BuildingPieceData[width, height];

                for (int x = 0; x != c_Map.c_Mapsize.x; x++)
                {
                    for (int y = 0; y != c_Map.c_Mapsize.y; y++)
                    {
                        //Creates a piece of land
                        GameObject go = Instantiate(m_BaseTerrainPiece, new Vector3(x, 0, y) + offSet, Quaternion.identity);

                        c_Map.c_Map[x, y] = go;

                        if (go.GetComponent<MapPieceData>())
                        {
                            MapPieceData mpd = go.GetComponent<MapPieceData>();
                            c_Map.c_TerrainDataMap[x,y] = mpd;
                            mpd.m_MapDataSObject = m_TerrainPrefabs[0];
                            mpd.m_pos = new Vector2(x, y);
                            
                            mpd.ApplyMaterial();
                            c_Map.c_TerrainDataMap[x, y] = mpd;
                            mpd.m_BuildingPieceData = mpd.GetComponent<BuildingPieceData>();
                            c_Map.c_BuildingMap[x,y] = mpd.m_BuildingPieceData;
                        }
                        go.name = "Map_" + x + "_" + y;
                        go.transform.parent = m_ParentFolder;
                    }
                }
            }
        }        
    }
}
