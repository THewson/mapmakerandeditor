﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Linq;

//  I'd prefer if you didn't touch this file, as it's a pain.
//  and I'd like to just work on it myself.

namespace mapping
{
    [RequireComponent(typeof(MapManager))]
    public class SaveDataManager : MonoBehaviour
    {
        public List<Map> savedMaps = new List<Map>();
        public MapManager man;

        // This is where the save files will be saved to.
        private string pathToSavedGamesFolder = "Assets/SavedGames";

        private void Start()
        {
            MaintainFile();
        }

        //Maintains the save file. Creating a new one if need be.
        public void MaintainFile()
        {
            if (!Directory.Exists(pathToSavedGamesFolder))
            {
                Directory.CreateDirectory(pathToSavedGamesFolder);
            }
        }
        
        // Unfinished file that would go through the save file and
        // see if there are any files with the same string name.
        public bool CompareFileNames(string saveFileName)
        {
            return false;
        }

        // Saves the file
        public void SaveMap(string saveFileName)
        {
            if (man.m_SaveLoadInputBox.text != "ERRORINVALIDSAVENAME321")
            {
                // create a new document
                XDocument document = new XDocument();

                // add a root item to it
                XElement root = new XElement("Map");

                // Make a new map
                Map map = man.c_Map;

                // Input Map Size
                XElement mapSize = new XElement("MapSize");
                mapSize.SetAttributeValue("X", map.c_Mapsize.x);
                mapSize.SetAttributeValue("Y", map.c_Mapsize.y);

                // Map Data Root 
                XElement terrainData = new XElement("MapData");
                XElement element = new XElement("MapData");

                for (int x = 0; x != map.c_Mapsize.x; x++)
                {
                    for (int y = 0; y != map.c_Mapsize.y; y++)
                    {
                        element = new XElement(map.c_Map[x, y].name);
                        // Adds the building growth
                        element.SetAttributeValue("BuildingGrowth", map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingGrowth);

                        // Adds the building piece assigned value
                        element.SetAttributeValue("BuildingValue", (int)map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingData.m_AssignedValue);

                        // Adds the assigned value of a terrain piece
                        element.SetAttributeValue("TerrainValue", (int)map.c_TerrainDataMap[x, y].m_MapDataSObject.m_AssignedValue);

                        // Adds the position of the terrain piece
                        element.SetAttributeValue("PosY", map.c_TerrainDataMap[x, y].m_pos.y);
                        element.SetAttributeValue("PosX", map.c_TerrainDataMap[x, y].m_pos.x);

                        terrainData.Add(element);
                    }
                }

                XElement resourceValues = new XElement("ResourceValues");

                for (int i = 0; i != man.m_ResourceManager.m_ResourceList.Count; i++)
                {
                    element = new XElement(man.m_ResourceManager.m_ResourceList[i].resource.ToString());

                    element.SetAttributeValue("Current", man.m_ResourceManager.m_ResourceList[i].resourceCurrent);
                    element.SetAttributeValue("Max", man.m_ResourceManager.m_ResourceList[i].resourceMax);
                    element.SetAttributeValue("Resource", man.m_ResourceManager.m_ResourceList[i].resource);

                    resourceValues.Add(element);
                }

                root.Add(mapSize);
                root.Add(terrainData);
                root.Add(resourceValues);

                document.Add(root);

                // save the document out to disc.
                document.Save(man.m_SaveLoadInputBox.text + ".xml");
            }
        }

        // Loads the file
        public void LoadMap()
        {
            XElement element = XElement.Load(man.m_SaveLoadInputBox.text + ".xml");

            Map map = man.c_Map;

            // find all region tags and read their attributes
            foreach (XElement item in element.Elements("MapSize"))
            {
                map.c_Mapsize.x = int.Parse(item.Attribute("X").Value);
                map.c_Mapsize.y = int.Parse(item.Attribute("Y").Value);
            }

            man.m_InputFieldWidth.text = man.c_Map.c_Mapsize.x.ToString();
            man.m_InputFieldHeight.text = man.c_Map.c_Mapsize.y.ToString();

            man.InitialiseNewMap();

            XElement mapDataNode = element.Element("MapData");

            // Loads map bits.
            if (mapDataNode != null)
            {
                for (int x = 0; x != map.c_Mapsize.x; x++)
                {
                    for (int y = 0; y != map.c_Mapsize.y; y++)
                    {
                        XElement node = mapDataNode.Element(map.c_Map[x, y].name);
                        if (node != null)
                        {
                            int terrainN = int.Parse(node.Attribute("TerrainValue").Value);
                            int buildinN = int.Parse(node.Attribute("BuildingValue").Value);
                            float buildinGrowth = float.Parse(node.Attribute("BuildingGrowth").Value);

                            map.c_TerrainDataMap[x, y].m_MapDataSObject = man.m_TerrainPrefabs[terrainN];
                            map.c_TerrainDataMap[x, y].ApplyMaterial();

                            map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingData = man.m_BuildingPrefabs[buildinN];
                            map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingGrowth = buildinGrowth;
                            map.c_TerrainDataMap[x, y].m_BuildingPieceData.CreateBuilding();
                        }
                    }
                }
            }

            // Loads resource bits
            XElement resourse = element.Element("ResourceValues");
            
            for (int i = 0; i != man.m_ResourceManager.m_ResourceList.Count; i++)
            {
                XElement node = resourse.Element(man.m_ResourceManager.m_ResourceList[i].resource.ToString());

                if(node.Name == man.m_ResourceManager.m_ResourceList[i].resource.ToString())
                {
                    int resourceMax = int.Parse(node.Attribute("Max").Value);
                    int current = int.Parse(node.Attribute("Current").Value);

                    man.m_ResourceManager.m_ResourceList[i].resourceMax = resourceMax;
                    man.m_ResourceManager.m_ResourceList[i].resourceCurrent = current;
                }
            }
        }
    }
}
