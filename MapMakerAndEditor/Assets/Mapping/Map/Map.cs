﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping
{
    [System.Serializable]
    public class Map
    {
        public Vector2 c_Mapsize;
        public GameObject[,] c_Map;
        public MapPieceData[,] c_TerrainDataMap;
        public BuildingPieceData[,] c_BuildingMap;
    }
}