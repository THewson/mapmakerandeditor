﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping
{
[CreateAssetMenu(fileName = "TerrainPrefab", menuName = "MapUtility/TerrainPrefabs", order = 1)]
    public class MapDataSObject : ScriptableObject
    {
        public TerrainEnum m_AssignedValue;
        public int m_MovementDifficulty;
        public Material m_Material;        
    }
}