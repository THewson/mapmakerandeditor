﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping
{
    public enum TerrainEnum
    {
        //IMPASSABLE = 0,
        GRASS,
        WATER,
        DEADGRASS
    }

    public enum BuildingEnum
    {
        EMPTY, //Do not change this element
        // Examples:
        HOUSE,
        STOREHOUSE
    }
    
    // Resource Types
    public enum ResourceType
    {
        // Examples:
        GOLD,
        ENERGY
    }
}