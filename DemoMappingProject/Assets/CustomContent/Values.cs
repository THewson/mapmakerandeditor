﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping {
    public enum TerrainEnum {
        //IMPASSABLE = 0,
        WATER,
        DEEPWATER,
        GRASS,
        DEADGRASS
    }

    public enum BuildingEnum {
        EMPTY, //DO NOT change this element
        HOUSE,
        MILL,
        BANK
    }

    // Resource Types
    public enum ResourceType {
        GOLD,
        WOOD,
        FOOD
    }
}