﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping {
    [CreateAssetMenu(fileName = "TerrainPrefab", menuName = "MapUtility/TerrainPrefabs", order = 1)]
    public class MapDataSObject : ScriptableObject {
        // The enum of the map piece. i.e. Dirt.
        public TerrainEnum m_AssignedValue;
        // How hard it is to move over
        public int m_MovementDifficulty;
        // Assigned material
        public Material m_Material;
    }
}