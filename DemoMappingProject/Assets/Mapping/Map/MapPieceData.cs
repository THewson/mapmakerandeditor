﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace mapping {
    [System.Serializable]
    public class MapPieceData : MonoBehaviour {
        //Relative Position in Space
        public Vector2 m_pos;
        public MapDataSObject m_MapDataSObject;
        [HideInInspector]
        public BuildingPieceData m_BuildingPieceData;

        private void Start() {
            // Automatically shoves EMPTY in, to avoid crashing errors
            if (m_BuildingPieceData.m_BuildingData == null) {
                Object obj = Resources.Load("EMPTY");

                m_BuildingPieceData.m_BuildingData = obj as BuildingDataSObject;
            }
        }

        // Assigns material to the map piece
        public void ApplyMaterial() {
            GetComponentInParent<MeshRenderer>().material = m_MapDataSObject.m_Material;
        }
    }
}