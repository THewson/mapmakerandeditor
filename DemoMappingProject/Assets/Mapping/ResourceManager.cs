﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mapping {
    [System.Serializable]
    // Container for a given resource type
    public class ResourceContainer {
        // Resource type.
        public ResourceType resource;
        // Resources current amount
        public int resourceCurrent;
        // Resource maximum capacity
        public int resourceMax;
        // The text box it'll be displayed on
        public Text text;
        public void AddResource(int res) {
            resourceCurrent += res;
        }
        public void AlterMaxResource(int res) {
            resourceMax += res;
        }
    }

    [System.Serializable]
    // struct for building types
    public struct Buildings {
        public BuildingEnum buildingType;
        public int income;
        public float tickTimer;
        [HideInInspector]
        public float timeRemaining;
    }

    public class ResourceManager : MonoBehaviour {
        [Header("Resource Lists")]
        public List<ResourceContainer> m_ResourceList;

        [Header("Building List")]
        public List<Buildings> m_buildingCategories;

        [Header("Resource Text Fields")]
        public List<Text> ResourceTextList;

        // Use this for initialization.
        void Start() { }

        // Update the relevant resources.
        public void UpdateResourceTicks(List<ResourceCost> resourceCostList) {
            // resourceCostList is a list of procking resources.

            for (int i = 0; i != m_buildingCategories.Count; i++) {

            }

            for (int i = 0; i != resourceCostList.Count; i++) {
                // If the resourceCost consumes resources it 
                // applies the inverse of the resource amount per tick.
                // i.e. 5 = -5
                if (resourceCostList[i].consumesResources) {
                    // A loop that goes through all the resources to find the one
                    // that matches the resourcecost type, 
                    // then applying the cost to the resource. 
                    for (int x = 0; x != m_ResourceList.Count; x++) {
                        if (m_ResourceList[x].resource == resourceCostList[i].resourceType) {
                            m_ResourceList[x].AddResource(-resourceCostList[i].amountPerTick);
                        }
                    }
                }
                // If it doesn't consume resources.
                else {
                    for (int x = 0; x != m_ResourceList.Count; x++) {
                        if (m_ResourceList[x].resource == resourceCostList[i].resourceType) {
                            m_ResourceList[x].AddResource(resourceCostList[i].amountPerTick);
                        }
                    }
                }
            }
        }

        // Adds to the max of the resource
        public void AddTotalResource(List<ResourceMaxBooster> list) {
            for (int i = 0; i != list.Count; i++) {
                for (int res = 0; res != m_ResourceList.Count; res++) {
                    if (list[i].resource == m_ResourceList[res].resource) {
                        m_ResourceList[res].resourceMax += list[i].NumberToAdd;
                        break;
                    }
                }
            }
        }

        // Inverse of the total resource
        public void RemoveTotalResource(List<ResourceMaxBooster> list) {
            for (int i = 0; i != list.Count; i++) {
                for (int res = 0; res != m_ResourceList.Count; res++) {
                    if (list[i].resource == m_ResourceList[res].resource) {
                        m_ResourceList[res].resourceMax -= list[i].NumberToAdd;
                        break;
                    }
                }
            }
        }

        private void LateUpdate() {
            for (int i = 0; i != m_ResourceList.Count; i++) {
                // Caps the resource.
                if (m_ResourceList[i].resourceCurrent > m_ResourceList[i].resourceMax) {
                    m_ResourceList[i].resourceCurrent = m_ResourceList[i].resourceMax;
                }

                // This will probably work.
                //if (m_ResourceList[i].resourceCurrent < 0)
                //{
                //    m_ResourceList[i].resourceCurrent = 0;
                //}

                // Updates the resource displayer
                m_ResourceList[i].text.text = System.Enum.GetName(typeof(ResourceType), m_ResourceList[i].resource) +
                    ": " + m_ResourceList[i].resourceCurrent + " / " + m_ResourceList[i].resourceMax;
            }
        }

        // Adds a certain amount to a given resource.
        //Each resource is checked against every avaliable resource to see if there is a match
        // The resource is then added if it's found.
        public void AddResource(ResourceType resource, int resourceToAdd) {
            int index = -1;
            for (int i = 0; i != m_ResourceList.Count; i++) {
                if (m_ResourceList[i].resource == resource) {
                    index = i;
                    if (m_ResourceList[i].resourceCurrent + resourceToAdd <= m_ResourceList[i].resourceMax) {
                        m_ResourceList[i].AddResource(resourceToAdd);
                    }
                }
            }
            if (index == -1) {
                Debug.Log("The resource you are trying to access doesn't exist.");
            }
        }

        // Removes a certain amount from a given resource.
        // Same as add, except it removes.
        public void RemoveResource(ResourceType resource, int resourceToRemove) {
            int index = -1;
            for (int i = 0; i != m_ResourceList.Count; i++) {
                if (m_ResourceList[i].resource == resource) {
                    index = i;
                    if (m_ResourceList[i].resourceCurrent != 0) {
                        m_ResourceList[i].AddResource(-resourceToRemove);
                    }
                }
            }
            if (index == -1) {
                Debug.Log("The resource you are trying to access doesn't exist.");
            }
        }

        // Print the resource list.
        public void PrintResourceList() {
            for (int i = 0; i != m_ResourceList.Count; i++) {
                Debug.Log(i + ": " + m_ResourceList[i].resource + " : " + m_ResourceList[i].resourceMax + " : " + m_ResourceList[i].resourceCurrent);
            }
        }
    }
}