﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//the save/load function needs updating.

namespace mapping {
    public class MapManager : MonoBehaviour {
        // MAP MANAGING //
        [Header("Prefabs")]
        // this is the base for all the land pieces. There is only one, and it musn't be changed.
        public GameObject m_BaseTerrainPiece;

        // These are the base prefabs of the entire list of buildings and land data.
        public MapDataSObject[] m_TerrainPrefabs;
        public BuildingDataSObject[] m_BuildingPrefabs;

        [Header("Input Fields")]
        // These are for making the map size/width
        public InputField m_InputFieldWidth;
        public InputField m_InputFieldHeight;

        // Saving and loading input field (for the name) 
        public InputField m_SaveLoadInputBox;

        [Header("Misc.")]
        public ResourceManager m_ResourceManager;
        
        // The parent folder to contain the map pieces.
        public Transform m_ParentFolder;

        private CameraController m_CameraController;
        public Transform m_Camera;

        // Camera/camera object offset
        public Vector3 offSet = new Vector3(3, 0, 3);

        public Map c_Map;

        void Start() {
            // Creating new map and getting the camera
            c_Map = new Map();
            if (m_Camera.GetComponent<CameraController>()) {
                m_CameraController = m_Camera.GetComponent<CameraController>();
            }
        }

        public string SaveLoadText() {
            if (m_SaveLoadInputBox.text == "") {
                return "ERRORINVALIDSAVENAME321";
            }
            else {
                return m_SaveLoadInputBox.text;
            }
        }

        // Calculate what resourcecosts are ready to proc
        void CalculateResourceUpdates() {
            List<ResourceCost> resourceCost = new List<ResourceCost>();

            for (int x = 0; x != c_Map.c_Mapsize.x; x++) {
                for (int y = 0; y != c_Map.c_Mapsize.y; y++) {
                    if (c_Map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingData.m_ResourcesToUpdate.Count > 0) {
                        if (c_Map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingGrowth == 1) {
                            foreach (ResourceCost rs in c_Map.c_TerrainDataMap[x, y].m_BuildingPieceData.m_BuildingData.m_ResourcesToUpdate) {
                                resourceCost.Add(rs);
                            }
                        }
                    }
                }
            }
            // Send that list to the resource manager
            m_ResourceManager.UpdateResourceTicks(resourceCost);
        }

        public void LateUpdate() {
            CalculateResourceUpdates();
        }

        // Destroys the current map
        public void DestroyMap() {
            while (m_ParentFolder.childCount != 0) {
                GameObject.DestroyImmediate(m_ParentFolder.GetChild(0).gameObject);
            }
        }

        // Initialise New Map
        public void InitialiseNewMap() {
            DestroyMap();
            //gets width/height from the input fields
            int width = int.Parse(m_InputFieldWidth.text);
            int height = int.Parse(m_InputFieldHeight.text);

            // Positions camera
            m_CameraController.SnapCameraToMapMiddle(width, height, offSet);

            c_Map.c_Mapsize.x = width;
            c_Map.c_Mapsize.y = height;

            // Creates map data structures
            c_Map.c_Map = new GameObject[width, height];
            c_Map.c_TerrainDataMap = new MapPieceData[width, height];
            c_Map.c_BuildingMap = new BuildingPieceData[width, height];

            for (int x = 0; x != c_Map.c_Mapsize.x; x++) {
                for (int y = 0; y != c_Map.c_Mapsize.y; y++) {
                    //Creates a piece of land
                    GameObject go = Instantiate(m_BaseTerrainPiece, new Vector3(x, 0, y) + offSet, Quaternion.identity);

                    c_Map.c_Map[x, y] = go;

                    if (go.GetComponent<MapPieceData>()) {
                        MapPieceData mpd = go.GetComponent<MapPieceData>();
                        c_Map.c_TerrainDataMap[x, y] = mpd;
                        mpd.m_MapDataSObject = m_TerrainPrefabs[0];
                        mpd.m_pos = new Vector2(x, y);
                        // Applies terrain material to the piece
                        mpd.ApplyMaterial();
                        c_Map.c_TerrainDataMap[x, y] = mpd;

                        // Applies correct map piece
                        mpd.m_BuildingPieceData = mpd.GetComponent<BuildingPieceData>();
                        c_Map.c_BuildingMap[x, y] = mpd.m_BuildingPieceData;
                    }
                    // Adds new land piece to the parent folder
                    go.name = "Map_" + x + "_" + y;
                    go.transform.parent = m_ParentFolder;
                }
            }
        }
    }
}