﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping {
    public class ChangeSelectedItem : MonoBehaviour {
        public MapDataSObject m_buttonMDSO;
        private GameObject m_camera;

        private void Start()
        {
             m_camera = GameObject.FindGameObjectWithTag("MainCamera");
        }

        public void itemClick()
        {
            if (m_camera.GetComponent<CameraController>())
            {
                m_camera.GetComponent<CameraController>().m_CurrentSelectedEnum = (int)m_buttonMDSO.m_AssignedValue;
            }
        }
    }
}