﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mapping
{
    public class OnClickSaveGame : MonoBehaviour
    {
        public Transform gameManager;
        public InputField path;

        public void clicked()
        {
            SaveDataManager dataManager = gameManager.GetComponent<SaveDataManager>();
            
            dataManager.SaveMap((string)path.text);
        }

        public void loadClicked()
        {
            SaveDataManager dataManager = gameManager.GetComponent<SaveDataManager>();

            dataManager.LoadMap();
        }
    }
}