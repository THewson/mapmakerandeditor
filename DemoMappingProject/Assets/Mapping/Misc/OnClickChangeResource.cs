﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mapping
{
    public class OnClickChangeResource : MonoBehaviour
    {
        public ResourceManager resourceManager;

        public void AddResource()
        {
            resourceManager.AddResource(0, 10);
        }

        public void RemoveResource()
        {
            resourceManager.RemoveResource(0, 10);
        }
    }
}