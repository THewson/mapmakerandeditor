﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace mapping
{
    public class CameraController : MonoBehaviour
    {
        public MapManager man;
        // Enums control both land and buildings
        // i.e. 1 = Grass / House
        public int m_CurrentSelectedEnum;
        // If place land is true, user clicks will place land, else they'll place buildings.
        public bool m_PlaceLand;
        
        // Movement
        public int m_CameraScrollingSpeed = 3;
        public int m_CameraRoatSpeed = 1;

        // Camera Variables
        private Camera m_Camera;
        Vector3 mouseLastPos;
        public float scrollSpeed = 1.0f;
        float cameraDistanceMax = 20.0f;
        float cameraDistanceMin = 5.0f;
        float cameraDistance = 10.0f;

        public EventSystem eventSystem;

        private void Start()
        {
            m_Camera = GetComponentInChildren<Camera>();
        }

        public void SnapCameraToMapMiddle(int x, int y, Vector3 offset)
        {
            Vector3 vec = new Vector3(x / 2, 0, y / 2) + offset;

            this.transform.position = vec;
        }

        public int GetLayerMaskBitWise(string name)
        {
            int layer = LayerMask.NameToLayer(name);
            int layerMask = 1 << layer;

            return layerMask;
        }

        private void LateUpdate()
        {
            // For getting the scroll wheel moving, and clamping a distance for it. 
            cameraDistance -= Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
            cameraDistance = Mathf.Clamp(cameraDistance, cameraDistanceMin, cameraDistanceMax);
            transform.position = new Vector3(transform.position.x, cameraDistance, transform.position.z);

            // Gets the camera relative movement keys. So left is left and right is right, relative to the camera
            Vector3 right = m_Camera.transform.right;
            Vector3 fwd = m_Camera.transform.forward;

            // Zeros Y to stop the camera moving up and down and normalises it.
            right.y = 0; right.Normalize();
            fwd.y = 0; fwd.Normalize();

            //Movement
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) { transform.position += fwd * Time.deltaTime * m_CameraScrollingSpeed; }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) { transform.position += right * Time.deltaTime * m_CameraScrollingSpeed; }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) { transform.position += -fwd * Time.deltaTime * m_CameraScrollingSpeed; }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) { transform.position += -right * Time.deltaTime * m_CameraScrollingSpeed; }

            //Right click to pan around cameras parent object
            if (Input.GetMouseButtonDown(1))
            { mouseLastPos = Input.mousePosition; }
            else if (Input.GetMouseButton(1))
            { transform.eulerAngles += Vector3.up * (Input.mousePosition.x - mouseLastPos.x) * m_CameraRoatSpeed * Time.deltaTime; }

            // Paint a piece of land on mouse click.
            if (Input.GetMouseButton(0))
            {
                if (!eventSystem.IsPointerOverGameObject())
                {
                    if (m_PlaceLand)
                    {
                        Ray ray = m_Camera.ScreenPointToRay(Input.mousePosition);
                        RaycastHit rayhit;

                        int landLayerMask = GetLayerMaskBitWise("Land");

                        if (Physics.Raycast(ray, out rayhit, 100000.0f, landLayerMask))
                        {
                            if (rayhit.transform.GetComponent<MapPieceData>())
                            {
                                if (m_PlaceLand)
                                {
                                    // Place a piece of land
                                    if ((int)rayhit.transform.GetComponent<MapPieceData>().m_MapDataSObject.m_AssignedValue != m_CurrentSelectedEnum)
                                    {
                                        rayhit.transform.GetComponent<MapPieceData>().m_MapDataSObject = man.m_TerrainPrefabs[m_CurrentSelectedEnum];
                                        rayhit.transform.GetComponent<MapPieceData>().ApplyMaterial();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Code concerning building construction on button click.
            if (Input.GetMouseButtonDown(0))
            {
                if (!eventSystem.IsPointerOverGameObject())
                {
                    if (!m_PlaceLand)
                    {
                        Ray ray = m_Camera.ScreenPointToRay(Input.mousePosition);
                        RaycastHit rayhit;

                        int landLayerMask = GetLayerMaskBitWise("Land");

                        if (Physics.Raycast(ray, out rayhit, 100000.0f, landLayerMask))
                        {
                            bool canPlace = false;
                            bool canAfford = true;
                            bool isEmpty = false;
                            
                            for (int i = 0; i != man.m_BuildingPrefabs[m_CurrentSelectedEnum].m_CompatableTerrainTypes.Count; i++)
                            {
                                if (rayhit.transform.GetComponent<MapPieceData>().m_MapDataSObject.m_AssignedValue == man.m_BuildingPrefabs[(int)m_CurrentSelectedEnum].m_CompatableTerrainTypes[i])
                                {
                                    canPlace = true;
                                }
                            }

                            if (rayhit.transform.GetComponent<MapPieceData>().m_BuildingPieceData.m_BuildingData.m_AssignedValue == (int)BuildingEnum.EMPTY)
                            {
                                isEmpty = true;
                            }

                            for (int i = 0; i != man.m_BuildingPrefabs[m_CurrentSelectedEnum].buildCosts.Length; i++)
                            {
                                for (int res = 0; res != man.m_ResourceManager.m_ResourceList.Count; res++)
                                {
                                    if (man.m_BuildingPrefabs[m_CurrentSelectedEnum].buildCosts[i].resource == man.m_ResourceManager.m_ResourceList[res].resource)
                                    {
                                        if (man.m_ResourceManager.m_ResourceList[res].resourceCurrent < man.m_BuildingPrefabs[m_CurrentSelectedEnum].buildCosts[i].costAmount)
                                        {
                                            canAfford = false;
                                        }
                                        break;
                                    }
                                }
                            }

                            if(m_CurrentSelectedEnum == (int)BuildingEnum.EMPTY) // AN empty can be placed at anytime.
                            {
                                isEmpty = true;
                                canAfford = true;
                                canPlace = true;
                            }

                            if (isEmpty)
                            {
                                if (canPlace)
                                {
                                    if (canAfford)
                                    {
                                        // Building is placed here.
                                        for (int i = 0; i != man.m_BuildingPrefabs[m_CurrentSelectedEnum].buildCosts.Length; i++)
                                        {
                                            for (int res = 0; res != man.m_ResourceManager.m_ResourceList.Count; res++)
                                            {
                                                if (man.m_BuildingPrefabs[m_CurrentSelectedEnum].buildCosts[i].resource == man.m_ResourceManager.m_ResourceList[res].resource)
                                                {
                                                    man.m_ResourceManager.RemoveResource(man.m_ResourceManager.m_ResourceList[res].resource, man.m_BuildingPrefabs[m_CurrentSelectedEnum].buildCosts[i].costAmount);
                                                }
                                            }
                                        }

                                        rayhit.transform.GetComponent<MapPieceData>().m_BuildingPieceData.m_BuildingData.OnDestroyed();
                                        rayhit.transform.GetComponent<MapPieceData>().m_BuildingPieceData.m_BuildingData = man.m_BuildingPrefabs[m_CurrentSelectedEnum];
                                        rayhit.transform.GetComponent<MapPieceData>().m_BuildingPieceData.CreateBuilding();

                                        if (rayhit.transform.GetComponent<MapPieceData>().m_BuildingPieceData.m_BuildingData is CapacityBuilding)
                                        {
                                            CapacityBuilding myThing = (CapacityBuilding)rayhit.transform.GetComponent<MapPieceData>().m_BuildingPieceData.m_BuildingData;
                                            if (myThing != null)
                                            {
                                                myThing.man = man;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
