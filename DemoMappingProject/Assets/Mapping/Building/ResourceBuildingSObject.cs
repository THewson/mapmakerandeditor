﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping {

    [CreateAssetMenu(fileName = "ResourceBuilding", menuName = "MapUtility/ResourceBuilding", order = 2)]
    public class ResourceBuildingSObject : BuildingDataSObject {

        public List<ResourceCost> m_ResourceList;

        [HideInInspector]
        public List<float> m_TimeUntilNextTick;

        override public void UpdateBuilding() {
            m_ResourcesToUpdate.Clear();

            for (int i = 0; i != m_ResourceList.Count; i++) {
                m_TimeUntilNextTick[i] -= Time.deltaTime / m_ResourceList[i].secondsPerTick;
                if (m_TimeUntilNextTick[i] < 0) {
                    m_TimeUntilNextTick[i] = 1;
                    m_ResourcesToUpdate.Add(m_ResourceList[i]);
                }
            }
        }

        override public void OnBuilt() {
            for (int i = 0; i != m_ResourceList.Count; i++) {
                m_TimeUntilNextTick.Add(1);
            }
        }
    }
}