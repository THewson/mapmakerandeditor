﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mapping {
    // Building SObject is a virtual class that for 
    public class BuildingDataSObject : ScriptableObject {
        // called in the late update
        public virtual void UpdateBuilding() { }
        // Called when the building is built
        public virtual void OnBuilt() { }
        // Called when a building is destroyed
        public virtual void OnDestroyed() { }
        // Calles when a building is upgraded
        public virtual void OnUpgrade() { }

        // How long it takes to grow
        public float m_SecondsToGrow;
        public BuildingEnum m_AssignedValue;
        public int m_MovementDifficulty;
        public GameObject m_BuildingObject;

        [Tooltip("If this is true, the program will skip terrain check.")]
        public bool m_IsUniversal;

        [HideInInspector]
        public MapManager man;
        
        public BuildingCost[] buildCosts;
        public List<ResourceCost> m_ResourcesToUpdate;
        public List<TerrainEnum> m_CompatableTerrainTypes;

        public virtual List<ResourceCost> ProcessCosts() { return null; }
    }
}