﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace mapping {
    [CreateAssetMenu(fileName = "EMPTY", menuName = "MapUtility/EmptyBuilding")]
    public class EmptyBuildingSObject : BuildingDataSObject {
        private void Start() {
            m_SecondsToGrow = 0;
            m_AssignedValue = 0;
            m_IsUniversal = true;
        }

        override public void UpdateBuilding() { }
        override public void OnBuilt() { }
        override public void OnDestroyed() { }
    }
}