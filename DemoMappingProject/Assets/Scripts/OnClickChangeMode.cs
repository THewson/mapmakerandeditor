﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickChangeMode : MonoBehaviour {

    public mapping.CameraController cam ;
    public bool newOne;

    public void onClick()
    {
        cam.m_PlaceLand = newOne;
    }
}
