﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickChangeDrawMode : MonoBehaviour {

    public mapping.CameraController cam;
    public bool placeMode;

    // Swaps the draw mode
    public void onClick()
    {
        cam.m_PlaceLand = placeMode;
    }

}
