﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseUnpause : MonoBehaviour {

    public float timeToSet;
    
    public void OnClick()
    {
        Time.timeScale = timeToSet;
    }
}
