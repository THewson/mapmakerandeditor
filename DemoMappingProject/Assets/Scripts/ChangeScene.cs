﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {
    public int sceneNum;

    public void OnClickChangeScene()
    {
        SceneManager.LoadScene(sceneNum);
    }
}
