﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClickChangeMenu : MonoBehaviour {

    public Canvas CurrentCanvas;
    public Canvas NextCanvas;
    
    public void ButtonPressed()
    {
        CurrentCanvas.gameObject.SetActive(false);
        NextCanvas.gameObject.SetActive(true);
    }
}
