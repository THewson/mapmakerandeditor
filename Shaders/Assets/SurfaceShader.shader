﻿Shader "Custom/SurfaceShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Cube("Cubemap", CUBE) = "" {}
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Timer("Timer", Range(0, 100)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldRefl;
			float4 screenPos;
			float3 worldPos;
		};

		sampler2D _MainTex;
		samplerCUBE _Cube;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Timer;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			//clip(frac((IN.worldPos.y + IN.worldPos.z*0.5) * 10) - 0.5);
			
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * 0.5;
			o.Emission = texCUBE(_Cube, IN.worldRefl).rgb;

			// Metallic and smoothness come from slider variables
			//o.Metallic = _Metallic;
			//o.Smoothness = _Glossiness;
			//o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
